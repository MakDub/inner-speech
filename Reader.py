# coding=utf-8
import glob
import mne
import os
import re
from datetime import datetime
from itertools import chain, zip_longest

import numpy as np
import scipy.io
from sklearn import preprocessing


def read(dir, sub, par_targets=[2, [1, 2, 3], 1], metadata=False):
    mat = scipy.io.loadmat(os.path.join(dir, sub))
    targets = np.ravel(mat.get('EEG')[:, -2])  # таргеты слов
    targets_act = np.ravel(mat.get('EEG')[:, -3])  # таргет мысленного/реального действия
    targets_art = np.ravel(mat.get('EEG')[:, -1])  # таргет наличия артефакта
    data = np.array([np.array(zip(*[iter(mat.get('EEG')[:, :len(mat.get('EEG')[0]) - 3][event])] * int(
        len(mat.get('EEG')[:, :len(mat.get('EEG')[0]) - 3][0]) / 6)))
                     for event in range(len(mat.get('EEG')))])  # 3d array data
    os.chdir(dir)
    if metadata == True:
        for file in glob.glob("*.txt"):
            f = open(os.path.join(dir, file), "r")
            meta = f.read()
    if len(par_targets) != 0:  # параметры таргетов, указанных в протоколе
        filtr_targets = np.zeros(targets.shape)
        masks = [[map(lambda y: targets[x] == y, par_targets[1]) for x in range(len(targets))]]
        for i, j in zip(masks[0], range(len(masks[0]))):
            if sum(i * 1) != 0:
                filtr_targets[j] = 1
        mask = (np.array(targets_act == par_targets[0]) * 1) * (np.array(targets_art == par_targets[2]) * 1) * (
                    filtr_targets * 1)
        mask = np.array(mask).astype(bool)
    if len(par_targets) == 0:
        return data, targets, targets_act, targets_art
    else:
        return data[mask], targets[mask], targets_act[mask], targets_art[mask], meta


# data,event,event_act,event_art = read(dir='C:\Users\MarkD\PycharmProjects\Inner speach\Base_de_Datos_Habla_Imaginada\S02',sub='S02_EEG.mat',par_targets=[1, [1,2,3], 1])
# read(директория, название файла, [мысленные/реальные,[через запятую активности],артефакты/без артефактов]; если par_target не указан, то вернет все данные
# за информацией о таргетах обращайтесь к протоколу в гугл папке

def cnt_read(dir, sub, ep_sub, labels, trials):
    data_loc = os.path.join(dir, sub)
    ep_loc = os.path.join(dir, ep_sub)
    raw = mne.io.read_raw_cnt(input_fname=data_loc, montage="standard_1020", preload=True)
    ep = scipy.io.loadmat(ep_loc)
    data = raw.get_data()
    target = np.genfromtxt(labels, dtype=np.str)
    data_3d = np.array([data[:, i[0][0]:i[0][0] + 4900] for i in ep[trials][0]])
    data_3d = data_3d[:, :64, :]
    le = preprocessing.LabelEncoder()
    le.fit(np.unique(target))
    target = le.transform(target)
    return data_3d, target


# data, target = cnt_read(dir = './', sub='Acquisition 232 Data.cnt',ep_sub='epoch_inds.mat',labels='labels.txt',trials='thinking_inds')
# print data, target

def EDF_data_reader(dir='./', file_name='Anton', mode='Naive', voice_tipe='All', add_mio=False):
    format = '%H:%M:%S.%f'
    i = 0  # счетчик количества серий, каждый слайд = 1 секунде на каждое слово (эвент) приходится 3 секунды
    k = 1
    Word = []  # для сбора значений будучих меток - вперед, влево, вправо, вниз, назад, вверх
    Time = []  # для сбора временных меток, чтобы выкинуть закрытые, открытые глаза в начале и в конце
    flag = 0
    text = []
    Rest_time = [[], []]
    history = np.zeros((3)) # чтобы учитывать непредсказуемые разрывы экспериментов - нужно иметь некоторую историю
    history_num=0
    break_point_num=[]
    break_point_slide=[]
    time_break_point=[]
    break_point_text=[]
    #slide_history=np.zeros((2))
    slide_history_num=0
    chek_break_flag=[]
    Rest_time_text = []
    for line in reversed(open(dir + file_name[:len(file_name) - 4] + '.txt').readlines()):  # паршу txt файл с конца

        regexp = re.compile(u'Слайд\w\w')  # ищу в линиях слово "Слайд" и две цифры после него
        slide = re.findall(regexp, line)  #




        if line.find("[")!=-1: # если в строке есть [], значит эксперимент уже начался

            history[history_num]=True  # в массиве истории отмечаем, что он идет непрерывно, до этого массив равен нулям
            history_num+=1
            if history_num==3:
                history_num=0

        else:  # если эксперимент прервался
            history[history_num] = False # отмечаем прерывание эксперимента
            history_num += 1
            if history_num == 3:
                history_num = 0

        if line.find(u'Разрыв') != -1:# ищу разрывы в экспериментах, разрывы не всегда длятся 1 секунду
             # если эксперимент прервался, но продолжился значит это разрыв внутри эксперимента

            time_break_point.append(line)


        if len(slide) != 0:  # если слово найдено то
            if slide_history_num==2:
                slide_history_num=0

            slide_num = slide[0][5:]  # выбрасываю слово и оставляю только номер слайда
            event = line[line.find("(") + 1:line.find(")")]  # в скобках данной линии указывается метка, нахожу ее
            num = line[line.find("[") + 1:line.find("]")]

            break_point_slide.append(int(slide_num))
            break_point_num.append(int(num))
            break_point_text.append(line)
            # slide_history[slide_history_num]=int(slide_num)
            # print(slide_history)
            # slide_history_num+=1


            if int(
                    slide_num) == 61:  # в каждой "серии" эксперимента 18 раз чередуется одно слово, каждое слово занимает 3 слайда(4с)
                Rest_time[1].append(
                    datetime.strptime(line[:12], format))  # Rest_time.append(datetime.strptime(line[:12], format))
                print (line)
                Rest_time_text.append(slide_num)
                i += 1
            k += 1
            if int(slide_num) == 1 and int(num) != 2:
                print (line)
                Rest_time[0].append(datetime.strptime(line[:12], format))
                Rest_time_text.append(slide_num)
                try:
                    if Rest_time_text[-1]==Rest_time_text[-2]:
                        chek_break_flag.append(True)
                    else:
                        chek_break_flag.append(False)
                except:
                    'первое вхождение'
            Word.append(event)  # добавляю в список найденные метки

        if 'ФП' in line or 'Фон1.jpg' in line:  # если в линии есть данные слова, значит эксперимент еще не начался
            if 'Фон1.jpg' in line:
                Time.append(datetime.strptime(text[flag - 1][:12], format))
            Time.append(
                datetime.strptime(line[:12], format))  # добавляю в Time временные метки не начавшегося эксперимента
            Time.sort()  # сортирую от меньшего к большему так как файл читается с конца

        if '<Все служебные маркеры>' in line:  # После этой строчки идут данные ЭЭГ записи, прерываю цикл
            break
        text.append(line)
        flag += 1
    break_point_slide.reverse()
    break_point_num.reverse()
    break_point_text.reverse()
    text.reverse()
    #chek_break_flag.reverse()
    break_point_num = np.array(break_point_num)
    break_point_slide = np.array(break_point_slide)


    mask = np.array([break_point_num[x]+1!=break_point_num[x+1] for x in range(len(break_point_num)-1)])
    print(break_point_num[:len(break_point_num) - 1][mask])
    left_break_marge_slide = np.array([break_point_slide[j] for j in
     [np.where(break_point_num == break_point_num[:len(break_point_num) - 1][mask][i])[0][0]
      for i in range(len(break_point_num[:len(break_point_num) - 1][mask]))]])

    right_break_marge_slide = np.array([break_point_slide[j + 1] for j in
     [np.where(break_point_num == break_point_num[:len(break_point_num) - 1][mask][i])[0][0]
      for i in range(len(break_point_num[:len(break_point_num) - 1][mask]))]])
    print (left_break_marge_slide)
    print (right_break_marge_slide)
    print (time_break_point, 'время разрыва', len(time_break_point),len(right_break_marge_slide))
    print (len(text[11:len(text)-4]),len(break_point_slide)+len(time_break_point)+11+4)
    mask_for_time_marg= np.zeros((len(right_break_marge_slide)))
    for l,r,ind in zip(left_break_marge_slide,right_break_marge_slide,range(len(right_break_marge_slide))):  # сделал маску для моментов верхней границы разрыва в конце серии
        if l>r:
            mask_for_time_marg[ind] = True
        else:
            mask_for_time_marg[ind] = False
    print (len(mask_for_time_marg.astype(bool)))
    time_left_break_marge = np.array([text[11:len(text) - 4][left_break_marge_slide[i]] for i in range(len(left_break_marge_slide))])
    print(left_break_marge_slide[mask_for_time_marg.astype(bool)])
    print(break_point_num[:len(break_point_num) - 1][mask][mask_for_time_marg.astype(bool)])
    break_point_time_marge = []#np.zeros((len(mask_for_time_marg)))
    for x,num_x in zip(break_point_num[:len(break_point_num) - 1][mask][mask_for_time_marg.astype(bool)],range(len(mask_for_time_marg))):
        for y in break_point_text:
            if '['+str(x)+']' in y:
                break_point_time_marge.append(datetime.strptime(y[:12], format))
    break_point_time_marge=np.array(break_point_time_marge)  # то время, которое придет на замену времени 61 слайда
    print (break_point_time_marge,'break_point_time_marge')
    raw = mne.io.read_raw_edf(input_fname=dir + file_name[:len(file_name) - 4] + '.EDF', preload=True)
    print (raw.info['ch_names'],len(raw.info['ch_names']))
    Time_start = Time[:8]  # в начале файла 8 временных значений перед экспериментом
    Time_end = Time[8:]  # остальные значения после эксперимента
    Rest_time[1].pop(0)

    print (len(Rest_time[0]))
    print(len(Rest_time[1]))
    start = 0
    chek_break_flag = np.pad(chek_break_flag, (0, 1), 'constant', constant_values=(0))
    chek_break_flag = np.array(chek_break_flag)[1:]
    time_break_point = np.flip(np.array(time_break_point))
    correct_time = np.zeros((len(chek_break_flag))).astype(datetime)
    print (mask_for_time_marg,'vvvvvvvvvvvvvvvvvv',break_point_time_marge)
    correct_time[chek_break_flag] = np.flip(break_point_time_marge)
    chek_break_flag = chek_break_flag*1
    print (chek_break_flag)
    correct_time[chek_break_flag==0]=Rest_time[1]
    Rest_time[1] = correct_time
    print (Rest_time[1])
    # for break_time_ in break_point_time_marge:
    #     #num = 0
    #     for num in range(start,len(Rest_time[1])):
    #         if break_time_> Rest_time[1][num]:
    #             print (str(break_time_),str(Rest_time[1][num]),num+1)
    #             start = num+1
    #             break
            #num += 1
    Rest_time = np.flip(np.array([(((Rest_time[0][i] - Rest_time[1][i]).total_seconds())) for i in range(len(Rest_time[0]))]))
    print (Rest_time)
    Rest_time = Rest_time * 250
    Rest_time = np.insert(Rest_time, 0, 0)
    # позиционирую точку среза начала эксперимента по секундам и милисекундам: из времени начала записи вычитаю время начала эксперимента и перевожу в отчеты
    Time_slice_start = int((Time_start[-1] - Time_start[0]).total_seconds()) * 250 + int(
        float(str((Time_start[-1] - Time_start[0]).total_seconds() % 1)[:5]) * 250) - 250
    # позиционирую точку конца эксперимента в секундах  и перевожу в отчеты
    Time_slice_end = int((Time_end[-1] - Time_end[0]).total_seconds()) * 250
    data = raw.get_data()
    if len(raw.info['ch_names']) > 19:  # если количество каналов больше 19 значит в эксперименте есть миограмма
        mio = np.array([data[-1, :]])
        mio = mio[:, Time_slice_start:mio.shape[1] - Time_slice_end]  # вырезаю данные начала и конца из ЭМГ
    data = data[:-3, :]

    data = data[:, Time_slice_start:data.shape[1] - Time_slice_end]  # вырезаю данные начала и конца из ЭЭГ

    Word.reverse()  # Сортирую метки(слова) в обратном порядке так как они считывались с конца файла
    le = preprocessing.LabelEncoder()
    le.fit(np.unique(Word))
    target = le.transform(Word)  # Из слов получаем числа
    # счетчики для того чтобы каждый шаг цикла сдвигать массив на серию вперед
    fit_target = 0  # счетчик для таргетов (предназначен для срезов из ЭЭГ)
    fit_event = 0  # счетчик для эвентов (для срезов из массива эвентов(слов))
    Data = []  # массив для накопления данных
    Event = []  # массив для накопления эвентов(слов)
    if len(raw.info['ch_names']) > 19:
        Mio = []
    else:
        Mio = []  # массив для накопления данных миограммы
    start_target_out = [6, 12, 18, 24, 30, 36, 42, 48,
                        54]  # list(range(6,54,6))  # начальная последовательность точек для срезов, стартовые точки среза реальной речи, она идет первой
    end_target_out = [9, 15, 21, 27, 33, 39, 45, 51, 57]  # list(range(9, 57, 6))  # конечные точки срезов реальной речи
    start_target_inner = [9, 15, 21, 27, 33, 39, 45, 51,
                          57]  # list(range(9, 57, 6))  # стартовые точки для срезов внутренней речи
    end_target_inner = [12, 18, 24, 30, 36, 42, 48, 54,
                        60]  # list(range(12, 61, 6))  # конечные точки для срезов внутренней речи

    for j in range(i):  # запускаем цикл по набранному счетчику
        if j > 0:
            fit_event = inner_voice_event[-1][-1] + 1
        inner_voice_target = np.array([range(start, end + 1) for start, end
                                       in zip(start_target_inner,
                                              end_target_inner)]) * 250 + fit_target
        inner_voice_event = np.array([range(start, end + 1) for start, end
                                      in zip(start_target_inner,
                                             end_target_inner)]) + fit_event  # последовательность эыентов заканчивается на произнесении слова внутренним голосом
        # сдвижка происходит на fit_target и fit_event каждый шаг цикла, fit_target и fit_event принимают
        # значения последней точки среза в конце цикла и накапливаются в ходе цикла
        #
        voice_event = np.array([range(start, end + 1) for start, end
                                in zip(start_target_out,
                                       end_target_out)]) + fit_event

        if j > 0:
            inner_voice_target = inner_voice_target + Rest_time[j]  # 6 секунд отдыха между сериями

        if voice_tipe == 'All':  # выбрать из файла реальный голос и внутренний голос

            voice_target = np.array([range(start, end + 1) for start, end
                                     in zip(start_target_out,
                                            end_target_out)]) * 250 + (fit_target)

            if j > 0:
                voice_target = voice_target + Rest_time[j]  # 6 секунд отдыха между сериями
            ep = np.array(
                ([ele for ele in chain.from_iterable(zip_longest(voice_target, inner_voice_target)) if
                  ele is not None]))  # для формирования вектора данных
            ep_event = np.array(
                (
                    [ele for ele in chain.from_iterable(zip_longest(voice_event, inner_voice_event)) if
                     ele is not None]))  # для формирования вектора меток
        if voice_tipe == 'out':  # только реальный голос
            voice_target = np.array([range(start, end + 1) for start, end
                                     in zip(start_target_out,
                                            end_target_out)]) * 250 + (fit_target)

            if j > 0:
                voice_target = voice_target + Rest_time[j]  # 6 секунд отдыха между сериями
            ep = np.array(voice_target)
            ep_event = np.array(voice_event)
        if voice_tipe == 'inner':  # только внутренний голос
            ep = np.vstack(inner_voice_target)
            ep_event = np.vstack(inner_voice_event)

        data_3d = np.array([data[:, int(i[0]):int(i[-1])] for i in ep])  *(10**6) # перевод данных в 3-ехмерный вид
        if len(raw.info['ch_names']) > 19:
            mio_3d = np.array([mio[:, int(i[0]):int(i[-1])] for i in ep])
            Mio.append(mio_3d)
        words = np.array([target[int(i[0]):int(i[-1])][0] for i in ep_event])  # формируется вектор меток

        Event.append(words)
        Data.append(data_3d)

        fit_target = inner_voice_target[-1][-1]  # накапливание "сдвижки" по ЭЭГ
        fit_event = inner_voice_event[-1][-1]  # накапливание "сдвижки" по массиву эвентов
    # объединение ЭЭГ, мио и эвентов

    Data = np.vstack(Data)
    if len(raw.info['ch_names']) > 19:
        Mio = np.vstack(Mio)
    Event = np.concatenate(Event)
    if mode == 'Naive':
        if add_mio == True and len(raw.info['ch_names']) > 19:
            Data = np.hstack([Data, Mio])
            return Data, Event
        else:
            return Data, Event, Mio[:, 0, :]

    if mode == 'mne':
        Data_mne = np.hstack([(i) for i in Data[:, :, :]])
        if len(raw.info['ch_names']) > 19:
            Mio_mne = np.hstack([(i) for i in Mio[:, :, :]])
        else:
            Mio_mne = [None]
        stimul = np.ones(Data_mne.shape[1])
        start = 0
        for i in range(Data.shape[0]):
            end = start + Data.shape[2]
            for j in range(start, end):
                stimul[j] = Event[i]
            start = end
        if add_mio == True and len(raw.info['ch_names']) > 19:
            Data_mne = np.vstack([Data_mne, Mio_mne])
            return Data_mne, stimul
        else:
            return Data_mne, stimul, Mio_mne
#X, y, m = EDF_data_reader(file_name='Anton.EDF', mode='Naive', voice_tipe='inner', add_mio=False)
def EDF_ENC_data_reader(dir='./', file_name='Anton', mode='Naive', voice_tipe='All', add_mio=False):
    Time=[]
    Number = []
    Slide = []
    Event = []
    State = []
    regexp = re.compile(u'Слайд\w\w')  # ищу в линиях слово "Слайд" и две цифры после него
    format = '%H:%M:%S.%f'
    for line in reversed(open(dir + file_name[:len(file_name) - 4] + '.txt').readlines()):
        if 'Время' in line:  # После этой строчки идут данные ЭЭГ записи, прерываю цикл
            break
        slide = re.findall(regexp, line)  #
        #print(slide)
        #print(line[line.find("[") + 1:line.find("]")],'<<<<<<')  # начинается всегда с [2]
        Time.append(line[:12])
        if len(slide)!=0:
            slide_num = slide[0][5:]  # выбрасываю слово и оставляю только номер слайда
            Slide.append(slide_num)
            event = line[line.find("(") + 1:line.find(")")]  # в скобках данной линии указывается метка, нахожу ее
            Event.append(event)
        if '[' in line:
            Number.append(line[line.find("[") + 1:line.find("]")])
        if 'ФП' in line or 'Ст' in line or 'Р' in line:
            State.append(line[12:15].replace("\t", ""))
    Number = np.flip(Number)
    Time = np.flip(Time)
    Slide = np.flip(Slide)
    Event = np.flip(Event)
    State = np.flip(State)
    # for string,num in zip(State,range(len(State))):
    #     print (string)
    #     State[num] = string.decode('string_escape')
    start = 10
    #print (Time[start])

    if State[start]==u'Р':
        while True:
            start+=1
            #print (start)
            if State[start] != u'Р':
                break
    background =[[0,(datetime.strptime(Time[start], format)-datetime.strptime(Time[0], format)).total_seconds(),64]]
    #print (Number[1])
    loop = 0
    point = 0
    data = []
    for n,t,s,e,st in zip(range(1,len(Number)),range(start,len(Time)),range(len(Slide)),range(len(Event)),range(start,len(State))):
        #print(Number[n],Time[t],Slide[s],Event[e],State[st])
        if int(Slide[s])<=6:
            #print (int(Slide[s]))
            data.append(Time[t])
            if len(data)==6:
                loop+=1
                print (loop,Slide[s],'<<<<<<<<<<<<')
                background.append([background[loop-1][1],background[loop-1][1]+(datetime.strptime(data[-1], format)-datetime.strptime(data[0], format)).total_seconds(),64])
                data=[]
                print (background)
        else:
            if State[st]!= u'Р':
                loop += 1
                print(Time[t-1],Time[t],Slide[s],State[st],loop)


    return