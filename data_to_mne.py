import mne
import numpy as np
import Reader


def info_events(meta, targets,freq,targ_seq, len_stim):
    """
    Auxiliary function for data_to_epochs() and data_to_raw()
    """
    meta = np.array([line.split('=') for line in meta.split('\n')])
    esp_eng = {'Diestro': 'right-handed', 'zurdo': 'left-handed'}
    descr = ', '.join([esp_eng.get(i, i) for i in meta[:, 1]])
    ch_names = ('F3', 'F4', 'C3', 'C4', 'P3', 'P4')
    ch_type = ['eeg'] * (len(ch_names))
    montage = mne.channels.read_montage('standard_1020', ch_names)
    info = mne.create_info(ch_names, sfreq=freq, ch_types=ch_type, montage=montage)
    info['description'] = descr
    event_ids = {'a': 1, 'e': 2, 'i': 3, 'o': 4, 'u': 5, 'up': 6, 'down': 7, 'right': 8, 'left': 9, 'forward': 10,
                 'backward': 11}
    for i, j in event_ids.items():
        if j not in targets:
            del event_ids[i]
    events, times = np.array([0, 0, targ_seq[0]]).reshape(1, 3), np.array([i * len_stim for i in range(1, targ_seq.shape[0])])
    events = np.vstack((events, np.hstack((times.reshape(times.shape[0], 1),
                                           targ_seq[:-1].reshape(targ_seq[:-1].shape[0], 1),
                                           targ_seq[1:].reshape(targ_seq[1:].shape[0], 1)))))
    return info, event_ids, events.astype(int)


def data_to_epochs(dir, sub, par_targets=[1, list(range(1, 12, 1)), 1]):
    """
    Returns mne.Epochs object made of array of single .mat file
    :param dir:  file path
    :param sub: file name
    :param par_targets: [a, [*sequence], b]
    a - 1 or 2, imagined or real stimuli; *sequence - a series of integers in range [1, 11] or just any of these numbers;
    b - 1 or 2, don't or do include stimuli with artifacts, respectively
    :return: mne.Epochs objects
    """
    data, targets, targets_act, targets_art, meta = Reader.read(dir, sub, par_targets=par_targets, metadata=True)
    info, event_ids, events = info_events(meta, par_targets[1], targets, data.shape[2])
    return mne.EpochsArray(data, info, events=events, event_id=event_ids, verbose=False)


def data_to_raw(dir, sub, par_targets=[1, list(range(1, 12, 1)), 1]):
    """
    :return: mne.raw_object, events (2D-array with following columns: event onset - previous event id - new event id),
    events_ids - dictionary with events names and corresponding ids.
    The rest, including call signature, see in data_to_epochs.__doc__
    '"""
    data, targets, targets_act, targets_art, meta = Reader.read(dir, sub, par_targets=par_targets, metadata=True)
    info, event_ids, events = info_events(meta, par_targets[1], targets, data.shape[2])
    channels = np.empty((data.shape[1], 0))
    for i in range(data.shape[0]):
        channels = np.hstack((channels, data[i]))
    raw = mne.io.RawArray(channels, info, verbose=False)
    return raw, events, event_ids
    

'''
Exemplary call signatures:
raw, events, event_ids = data_to_mne.data_to_raw(dir='A:\Inner speech\Argentina\S01', sub='S01_EEG.mat',
                                                 par_targets=[1, list(range(1, 12)), 1])

raw, events, event_ids = data_to_mne.data_to_raw(dir='A:\Inner speech\Argentina\S01', sub='S01_EEG.mat',
                                                 par_targets=[1, [2, 3, 6, 11], 1])
                                            
Totally the same for data_to_epochs()
'''