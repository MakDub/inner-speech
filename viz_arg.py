import matplotlib.pyplot as plt
import mne
import numpy as np
from mne.time_frequency import tfr_morlet
from scipy.stats import zscore

#ID_EVENTS = {1: 'a', 2: 'e', 3: 'i', 4: 'o', 5: 'u', 6: 'up', 7: 'down', 8: 'right', 9: 'left', 10: 'forward', 11: 'backward'}

def psd_pairs(epochs1, epochs2, targs, fmax=40):
    picks = mne.pick_types(epochs1.info, eeg=True, bio=False)
    plt.Figure(dpi=80)
    targ_ids = epochs1.event_id.keys()
    for i in range(len(targs)):
        ax = plt.subplot(len(targs)//2 + int(len(targs)%2!=0), 2, i+1)
        epochs1[targ_ids[i]].plot_psd(fmax=fmax, picks=picks, ax=ax, color='k', show=False, verbose=False)
        epochs2[targ_ids[i]].plot_psd(fmax=fmax, picks=picks, ax=ax, color='b', show=False, verbose=False)
        ax.legend(['Real \"{}\"'.format(targ_ids[i]), 'Imagined \"{}\"'.format(targ_ids[i])])
    plt.suptitle('PSDs of real and imagined words/sounds', fontsize=13)
    plt.tight_layout()
    plt.show()

def psd_interstim(sounds_real, sounds_im, words_real, words_im, fmax=40):
    colors = ('r', 'g', 'b', 'k', 'orange', 'purple')
    to_plot = (sounds_real, sounds_im, words_real, words_im)
    fig = plt.Figure(dpi=100)
    for i, epochs in enumerate(to_plot):
        picks = mne.pick_types(epochs.info, eeg=True, bio=False)
        ax = plt.subplot(2, 2, i+1)
        for j, targ in enumerate(epochs.event_id.keys()):
            epochs[targ].plot_psd(fmax=fmax, picks=picks, ax=ax, color=colors[j], show=False, area_mode=None, verbose=False)
        ax.legend([stim for stim in epochs.event_id.keys()])
        ax.set_xlabel('Frequency, Hz', fontsize=14)
        if i == 0:
            ax.set_title('Real', fontsize=18)
            ax.set_ylabel('Sounds', fontsize=18)
        elif i == 1:
            ax.set_title('Imagined', fontsize=18)
        elif i == 2:
            ax.set_ylabel('Words', fontsize=18)
    plt.tight_layout()
    plt.show()


def ers_d(real_in, im_in, freq_range=(0, 5)):
    real, im = real_in.copy(), im_in.copy()
    fig = plt.Figure(dpi=100)
    picks = mne.pick_types(real.info, eeg=True)
    real_psd = 10 * np.log10(mne.time_frequency.psd_welch(real, fmin=freq_range[0], fmax=freq_range[1], n_fft=1024, n_per_seg=768, verbose=False)[0])
    im_psd = 10 * np.log10(mne.time_frequency.psd_welch(im, fmin=freq_range[0], fmax=freq_range[1], n_fft=1024, n_per_seg=768, verbose=False)[0])
    psd_min, psd_max = 0.2 * min(np.min(real_psd), np.min(im_psd)), 0.9 * max(np.max(real_psd), np.max(im_psd))
    for i, stim in enumerate(sorted(real.event_id.keys(), key=len)):
        real_psd, real_fqs = mne.time_frequency.psd_welch(real[stim], fmin=freq_range[0], fmax=freq_range[1], n_fft=4096, n_per_seg=768, verbose=False)
        im_psd, im_fqs = mne.time_frequency.psd_welch(im[stim], fmin=freq_range[0], fmax=freq_range[1], n_fft=4096, n_per_seg=768, verbose=False)
        for j in range(len(picks)):
            ax1 = plt.subplot(len(picks), len(real.event_id), i + j*len(real.event_id)*int(j != 0) + 1)
            ax1.set_ylabel('')
            ax1.plot(real_fqs, 10 * np.log10(real_psd).mean(0)[j, :], color='k', lw=0.7)
            ax1.plot(im_fqs, 10 * np.log10(im_psd).mean(0)[j, :], color='b', lw=0.7)
            ax1.set_ylim((psd_min, psd_max))
            ax1.yaxis.set_minor_locator(plt.MultipleLocator(5))
            ax1.grid(axis='y', which='both')
            ax1.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
            if i == 0:
                ax1.tick_params(labelleft=True, left=True)
                ax1.set_ylabel('Ch {}'.format(j+1), fontsize=14)
            if j == 0:
                ax1.set_title(stim, fontsize=12)
            if j == len(picks)-1:
                ax1.tick_params(labelbottom=True, bottom=True, labelsize=7)
                ax1.set_xticks(np.linspace(freq_range[0], freq_range[1], int(freq_range[1]-freq_range[0])+1).astype(int))
    plt.figtext(0.5, 0.04, 'Frequency, Hz')
    plt.suptitle('Spectral power (real - black, imagined - blue)', y=0.97)
    #plt.tight_layout()
    plt.show()


def wavelets(epochs, freq_range=(30, 40)):
    epochs = epochs.copy()
    fig = plt.Figure(dpi=100)
    picks = mne.pick_types(epochs.info, eeg=True)
    freqs = np.linspace(freq_range[0], freq_range[1], freq_range[1] - freq_range[0])
    n_cycles = freqs / 2.
    tfrs = {stim: tfr_morlet(epochs[stim], freqs=freqs, n_cycles=n_cycles, picks=picks, return_itc=False) for stim in epochs.event_id.keys()}
    pmin, pmax = [], []
    for i, stim in enumerate(epochs.event_id.keys()):
        pmin.append(np.min(tfrs[stim].data))
        pmax.append(np.max(tfrs[stim].data))
    cmin, cmax = min(pmin), max(pmax)
    for i, stim in enumerate(sorted(tfrs.keys(), key=len)):
        tfr = tfrs[stim]
        for j in range(len(picks)):
            ax = plt.subplot(len(picks), len(epochs.event_id), i + j * len(epochs.event_id) * int(j != 0) + 1)
            tfr.plot(picks=[j], baseline=None, vmin=cmin, vmax=cmax, mode='logratio', axes=ax, show=False, colorbar=False, verbose=False, title='')
            ax.set_ylabel('')
            ax.tick_params(left=False, bottom=False, labelleft=False, labelbottom=False)
            ax.set_xlabel('')
            if j == len(picks) - 1:
                ax.tick_params(bottom=True, labelbottom=True)
            if j == 0:
                ax.set_title(stim, fontsize=12)
            else:
                ax.set_title('')
            if i == 0:
                ax.tick_params(labelleft=True, left=True)
                ax.set_ylabel('Ch %d, Hz' % (j + 1))
    plt.figtext(0.5, 0.04, 'Time, s')
    plt.show()

def hjorth_2(X, D=None):
    if D is None:
        D = np.diff(X)
        D = D.tolist()
    D.insert(0, X[0])
    D = np.array(D)
    n = len(X)
    M2 = float(sum(D ** 2)) / n
    TP = sum(np.array(X) ** 2)
    M4 = 0
    for i in range(1, len(D)):
        M4 += (D[i] - D[i - 1]) ** 2
    M4 = M4 / n
    return np.sqrt(M2 / TP), np.sqrt(float(M4) * TP / M2 / M2)


def hjorth(m):
    res = np.empty((0, m.shape[1], 3))
    for i in range(len(m)):
        event = np.empty((0, 3))
        for j in range(len(m[i])):
            mob, comp = hjorth_2(m[i, j])
            case = np.array([np.var(m[i, j]), mob, comp])
            case = case[np.newaxis, :]
            event = np.append(event, case, axis=0)
        event = event[np.newaxis, :, :]
        res = np.append(res, event, axis=0)
    return res

def hjorth_polar_bars(epochs):
    event_id = epochs.event_id
    seq = epochs.events[:, 2]
    epochs = epochs.get_data()
    hj = hjorth(epochs)
    h = np.empty((hj.shape[0], hj.shape[1], 0))
    for k in range(hj.shape[2]):
        layer = np.empty((hj.shape[0], 0))
        for j in range(hj.shape[1]):
            layer = np.hstack((layer, ((hj[:, j, k] - np.min(hj[:, j, k]))/np.ptp(hj[:, j, k]))[:, np.newaxis]))
        h = np.dstack((h, layer[:, :, np.newaxis]))
    hs = {code: h[seq == code] for code in event_id.values()}
    stds = {code: np.std(hs[code], axis=0) for code in hs}
    plt.figure(dpi=100)
    angles = [np.pi * 11. / 6. - n * np.pi * 2. / 3. for n in range(3)]
    angles.append(angles[0])
    for  j, stim in enumerate(event_id.values()):
        pars = np.mean(hs[stim], axis=0)
        for i in range(epochs.shape[1]):
            ax = plt.subplot(epochs.shape[1], len(event_id.keys()), j + 1 + i * len(event_id.keys()) * int(i != 0), polar=True)
            plt.xticks(angles[:-1], ['', '', ''], size=14)
            ax.tick_params(labelleft=False)
            ax.set_rlabel_position(0)
            plt.ylim(0, 0.7)
            colors = ['mediumseagreen', 'mediumorchid', 'salmon']
            par = pars[i, :]
            for k in range(3):
                ax.bar(angles[k], par[k], color=colors[k], yerr=stds[stim][i, k])
    plt.subplots_adjust(left=0.01, right=0.99, bottom=0.01, top=0.96, hspace=-0.1, wspace=0.01)
    plt.suptitle('Activity - red, Mobility - green, Complexity - purple')
    plt.show()
'''
#Example
dir = 'A:\Inner speech\Argentina\S01'
sub = 'S01_EEG.mat'
targs = [6, 7, 11]
epochs_real = data_to_epochs(dir=dir, sub=sub, par_targets=[2, targs, 1])
epochs_im = data_to_epochs(dir=dir, sub=sub, par_targets=[1, targs, 1])
psd_pairs(epochs_real, epochs_im, targs)
'''