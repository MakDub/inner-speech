# coding=utf-8
import numpy as np
from sklearn.preprocessing import StandardScaler, LabelBinarizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score,accuracy_score,confusion_matrix
from feachers import sample_corr
import copy
from sklearn.preprocessing import label_binarize
from visual_func import curve_plot, confusion_plot, log_loss_plot,cv_ac_score_plot
from preprocessing import holdout_method, cross_val_method
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import log_loss
import pandas as pd
'''
Parameters:
data: 3D array
event: 1D array
feach_mode: 
'''
def simple_model(data,event,feach_mode=sample_corr,h_method=False, h_m_rand=False,c_v_method=True, plot=True,cv=3,cv_mode='MK_cv'):
    sc = StandardScaler()
    data_f = feach_mode(data)[0]
    data_sc = sc.fit_transform(data_f,event)
    y_un = np.unique(event)
    event_all = copy.deepcopy(event)
    data_all = copy.deepcopy(data_sc)
    event = label_binarize(event, classes=y_un)
    auc_score = np.zeros((len(event[0])))
    cm_score = np.zeros((len(event[0]),2,2))
    clf = LogisticRegression()
    if h_method==True:
        data_train, event_train, data_test, event_test, data_train_all, data_test_all, event_train_all, event_test_all\
            = holdout_method(data_sc,data_all,event_all,event,h_m_rand=h_m_rand)
    if c_v_method == True:
        score_m,scores = cross_val_method(data_all,event_all,clf,cv=cv,mode=cv_mode)
        if cv_mode == 'q-fold':
            cv_ac_score_plot(scores, score_m)
        if cv_mode == 'MK_cv':
            data
        return
    For_plot=np.zeros((len(event[0]),2,event_test[:,0].shape[0]))
    for i in range(len(event[0])):  # one_vs_all score
        clf.fit(data_train,event_train[:,i])
        preds = clf.predict(data_test)
        score = accuracy_score(event_test[:,i], preds)
        auc = roc_auc_score(event_test[:,i],preds)
        #print auc, 'roc auc score'
        auc_score[i]=auc
        For_plot[i][0],For_plot[i][1]=event_test[:,i],preds
        cm_one_vs_all = confusion_matrix(event_test[:,i], preds)
        cm_score[i]=cm_one_vs_all
    clf.fit(data_train_all, event_train_all)
    preds = clf.predict(data_test_all)
    preds_p = clf.predict_proba(data_test_all)
    score = accuracy_score(event_test_all, preds)
    conf_m = confusion_matrix(event_test_all, preds)
    f_score = precision_recall_fscore_support(event_test_all, preds, average='micro')
    #print preds
    ll_score = log_loss(event_test_all,preds_p,eps=1e-15)
    #print f_score
    if plot==True:
        curve_plot(For_plot,auc_score)
        confusion_plot(conf_m,cm_score,f_score)
        log_loss_plot(ll_score)
    #return event_test, preds