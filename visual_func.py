# coding=utf-8
import copy

import matplotlib.pyplot as plt
import mne
import numpy as np
from matplotlib.collections import LineCollection
from matplotlib.lines import Line2D
from mne.time_frequency import (tfr_morlet)
from scipy import signal, stats
from scipy.signal import butter, lfilter
from sklearn import decomposition
from sklearn.manifold import MDS
from sklearn.manifold import TSNE
from sklearn.metrics import roc_curve

from Norm import MidpointNormalize
from data_to_mne import data_to_epochs
# from cross_cor import corr
from feachers import sample_corr as corr, sample_hjorth as hj, fft as fft
from preprocessing import bust_classes


# уменьшение размерности данных общий вид
def compression(X, Y, mode='tsne', time=False, chunk=False, title='',target=[]):
    X = copy.deepcopy(X)
    tsne = TSNE(random_state=1)
    mds = MDS(random_state=1)
    pca = decomposition.PCA(n_components=2)
    fig, axes = plt.subplots(1, 1, figsize=(13, 10))
    if mode == 'tsne':
        X = tsne.fit_transform(X)
    if mode == 'mds':
        X = mds.fit_transform(X)
    if mode == 'pca':
        X = pca.fit_transform(X)
    if mode == 'clear':
        X = X
    y_un = np.unique(Y)
    colors = ['green', 'blue', 'red', 'black', 'purple', 'brown', 'pink', 'orange', 'yellow', 'coral', 'lime']
    if time == False:
        if len(target)==0:
            c = np.unique(Y)
        else:
            c = target
        for y, color in zip(c, range(len(c))):
            axes.scatter(X[Y == y][:, 0], X[Y == y][:, 1], color=colors[color])
        axes.legend(y_un)
        axes.set_title(mode + ' ' + title)
        plt.show()
    if time == True:
        c = np.unique(Y)
        colors = ['#7e1e9c', '#15b01a', '#0343df', '#ff81c0', '#653700', '#f97306', '#96f97b', '#c20078', '#6e750e']
        for j in range(len(chunk)):
            for y in c:
                axes.scatter(X[:, 0][sum(chunk[:j]):sum(chunk[:j + 1])][Y[sum(chunk[:j]):sum(chunk[:j + 1])] == y],
                             X[:, 1][sum(chunk[:j]):sum(chunk[:j + 1])][Y[sum(chunk[:j]):sum(chunk[:j + 1])] == y],
                             color=colors[j])
        legend_elements = [Line2D([0], [0], marker='o', color=colors[i], label=str(range(len(chunk))[i]),
                                  markerfacecolor=colors[i], markersize=10) for i in range(len(chunk))
                           ]
        axes.legend(handles=legend_elements, loc='best')
        axes.set_title(mode + ' ' + 'in time' + ' ' + title)
        plt.show()

    return


# compression(X, Y, mode='tsne', time=False, chunk=False) X - двумерный массив (n_samples, n_features), Y - вектор таргетов
# time = True/False окрас точек по времени или простое отображение, chunk - вектор размеров серий, mode = tsne/mds

# уменьшение размерности данных по сериям
def compression_by_ser(X, Y, mode='tsne', chunk=False, title=False):
    X = copy.deepcopy(X)
    tsne = TSNE(random_state=1)
    mds = MDS(random_state=1)
    pca = decomposition.PCA(n_components=2)
    colors = ['green', 'blue', 'red', '#5F04B4', '#ADFF2F', '#00FFFF', '#DA70D6', '#AC58FA', '#86B404', '#006400',
              '#6B8E23']
    c = np.unique(Y)
    c_all = 3
    r_all = (len(chunk) // c_all) + 2
    fig, axes = plt.subplots(c_all, r_all, figsize=(13, 10))

    Min_lim = []
    Max_lim = []
    Min_lim_y = []
    Max_lim_y = []
    if chunk == False:
        print('select series size vector')
    if mode == 'tsne':
        X = tsne.fit_transform(X)
    if mode == 'mds':
        X = mds.fit_transform(X)
    if mode == 'pca':
        X = pca.fit_transform(X)
    r = 0
    col = 0
    for j in range(len(chunk)):
        if col == 3:
            col = 0
            r += 1
        for y, cl in zip(c, range(len(c))):
            axes[r, col].scatter(X[:, 0][sum(chunk[:j]):sum(chunk[:j + 1])][Y[sum(chunk[:j]):sum(chunk[:j + 1])] == y],
                                 X[:, 1][sum(chunk[:j]):sum(chunk[:j + 1])][Y[sum(chunk[:j]):sum(chunk[:j + 1])] == y],
                                 color=colors[cl])
            Lim = axes[r, col].get_xlim()
            Lim_y = axes[r, col].get_ylim()
            Min_lim.append(Lim[0] - 5)
            Max_lim.append(Lim[1] + 5)
            Min_lim_y.append(Lim_y[0] - 5)
            Max_lim_y.append(Lim_y[1] + 5)
        col += 1

    r = 0
    col = 0
    for j in range(len(chunk)):
        if col == 3:
            col = 0
            r += 1
        axes[r, col].set_xlim(min(Min_lim), max(Max_lim))
        axes[r, col].set_ylim(min(Min_lim_y), max(Max_lim_y))
        col += 1
    fig.legend(c)
    fig.suptitle(mode + ' ' + 'in ser' + ' ' + title)
    plt.show()
    return


# compression_by_ser(X, Y, mode='tsne', chunk=False) X - двумерный массив (n_samples, n_features), Y - вектор таргетов
# chunk - вектор размеров серий, mode = tsne/mds

# отображение всех компонент вектора признаков
def all_comp_visual_smell(X, Y, targets=False, some_comp=False):
    smells = targets
    X = copy.deepcopy(X)
    X = np.transpose(X)
    if smells == False:
        print
        'Select smells'
        return
    smell1 = smells[0]
    smell2 = smells[1]
    if len(smells) == 3:
        smell3 = smells[2]
    if len(smells) == 4:
        smell3 = smells[2]
        smell4 = smells[3]
    if len(smells) == 5:
        smell3 = smells[2]
        smell4 = smells[3]
        smell5 = smells[4]
    if len(smells) == 6:
        smell3 = smells[2]
        smell4 = smells[3]
        smell5 = smells[4]
        smell6 = smells[5]
    colors = ['#7e1e9c', '#15b01a', '#0343df', '#ff81c0', '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e',
              '#7e1e9c', '#15b01a', '#0343df', '#ff81c0', '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e'
        , '#7e1e9c', '#15b01a', '#0343df', '#ff81c0', '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e', '#7e1e9c', '#15b01a', '#0343df', '#ff81c0',
              '#653700', '#e50000', '#95d0fc',
              '#029386', '#f97306', '#96f97b', '#c20078', '#6e750e']
    fig, axes = plt.subplots(1, 1, figsize=(13, 10), dpi=80)
    if some_comp == False:
        for i in range(len(X)):
            axes.plot(np.array(X)[i][np.array(Y) == smell1], linestyle='-', linewidth=1.0, color=colors[i])
            axes.plot(np.array(X)[i][np.array(Y) == smell2], linestyle='--', linewidth=1.0, color=colors[i],
                      label='_nolegend_')
            if len(smells) == 3:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-.', linewidth=1.0, color=colors[i],
                          label='_nolegend_')
            if len(smells) == 4:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color=colors[i])
            if len(smells) == 5:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color=colors[i])
            if len(smells) == 6:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color=colors[i])
                axes.plot(np.array(X)[i][np.array(Y) == smell6], linestyle='-', linewidth=1.0, color=colors[i])
            axes.set_title(str(i))
            if len(X) < 40:
                fig.legend(range(len(X)), loc='upper right', fontsize='x-small')
            else:
                fig.legend(range(40), loc='upper right', fontsize='x-small')
    else:
        for i in some_comp:
            axes.plot(np.array(X)[i][np.array(Y) == smell1], linestyle='-', linewidth=1.0, color='blue')
            axes.plot(np.array(X)[i][np.array(Y) == smell2], linestyle='-', linewidth=1.0, color='red')
            if len(smells) == 3:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
            if len(smells) == 4:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
            if len(smells) == 5:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
                axes.plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color='black')
            if len(smells) == 6:
                axes.plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
                axes.plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
                axes.plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color='black')
                axes.plot(np.array(X)[i][np.array(Y) == smell6], linestyle='-', linewidth=1.0, color='pink')
            axes.set_title(str(i))
        fig.legend(smells, loc='right', prop={'size': 10})
    plt.show()


# all_comp_visual_smell(X,Y, smells=False), X - двумерный массив (n_samples, n_features), Y - вектор таргетов
# smells - вектор кодов клапанов (минимальный размер 2, максимальный 3)

# отображение всех компонент вектора признаков в матрице графиков, где каждая ячейка это отдельная компонента
def visual_comp_matrix(X, Y, targets=False, title='', f_metadata=False):
    smells = targets
    X = copy.deepcopy(X)
    X = np.transpose(X)
    c_all = 6
    r_all = (len(X) // c_all) + 2
    fig, axes = plt.subplots(r_all, c_all, figsize=(2 * c_all, 2 * r_all), dpi=100)
    plt.subplots_adjust(hspace=0.8)
    smell1 = smells[0]
    smell2 = smells[1]
    if len(smells) == 3:
        smell3 = smells[2]
    if len(smells) == 4:
        smell3 = smells[2]
        smell4 = smells[3]
    if len(smells) == 5:
        smell3 = smells[2]
        smell4 = smells[3]
        smell5 = smells[4]
    if len(smells) == 6:
        smell3 = smells[2]
        smell4 = smells[3]
        smell5 = smells[4]
        smell6 = smells[5]
    r = 0
    c = 0
    for i in range(len(X)):
        axes[r, c].plot(np.array(X)[i][np.array(Y) == smell1], linestyle='-', linewidth=1.0, color='blue')
        axes[r, c].plot(np.array(X)[i][np.array(Y) == smell2], linestyle='-', linewidth=1.0, color='red')
        if len(smells) == 3:
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
        if len(smells) == 4:
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
        if len(smells) == 5:
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color='black')
        if len(smells) == 6:
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell3], linestyle='-', linewidth=1.0, color='green')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell4], linestyle='-', linewidth=1.0, color='brown')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell5], linestyle='-', linewidth=1.0, color='black')
            axes[r, c].plot(np.array(X)[i][np.array(Y) == smell6], linestyle='-', linewidth=1.0, color='pink')
        if f_metadata != False:
            axes[r, c].set_title(str(i) + ' ' + f_metadata[i], fontsize=9)
        else:
            axes[r, c].set_title(str(i), fontsize=9)
        axes[r, c].tick_params(labelsize=6)
        c += 1
        if c == 6:
            c = 0
            r += 1

    for i in range(r, r_all):
        for j in range(c, c_all):
            fig.delaxes(axes[i, j])
        c = 0

    fig.legend(smells, loc='right', prop={'size': 10})
    fig.suptitle('components in time' + ' ' + title)
    plt.show()
    return


# visual_comp_matrix(X,Y, smells=False), X - двумерный массив (n_samples, n_features), Y - вектор таргетов
# smells - вектор кодов клапанов (минимальный размер 2, максимальный 3)
def filter_func(data_1, l_b1=1, h_b2=0.2, l_b3=0.3, h_b4=0.4, filter_mode='None', spatial_filter='None', start_s=0,
                stop_s=5000, order=500):
    def butter_bandpass(lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a

    def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
        b, a = butter_bandpass(lowcut, highcut, fs, order=order)
        y = lfilter(b, a, data)
        return y

    if spatial_filter == 'car':
        for e in range(len(data_1)):
            data_1[e] = np.subtract(data_1[e], np.mean(data_1[e], axis=0))
    if filter_mode == 'None':
        data_1 = data_1
    if filter_mode == 'multiband':
        f1, f2 = l_b1, h_b2
        f3, f4 = l_b3, h_b4
        taps = signal.firwin(500, [f1, f2, f3, f4], pass_zero=False, fs=1000)
        data_1 = lfilter(taps, 1.0, data_1)
    if filter_mode == 'fir':
        f1, f2 = l_b1, h_b2
        taps = signal.firwin(order, [f1, f2], pass_zero=False, fs=1000)
        data_1 = lfilter(taps, 1.0, data_1)
    if filter_mode == 'bat':
        data_1 = butter_bandpass_filter(data_1, l_b1, h_b2, 1000, order=order)
    return data_1


def distance_visual(data, event, normal='comp', one_comp=False, start=1, stop=140, step=10, fit=30, filter='fir',
                    s_filter='None', order=500, track=False, train=None, smell=[4, 8], feach_function=corr,
                    hand_band_mode=False, mode=False):
    data = copy.deepcopy(data)
    filter_data_f = []
    count_comp = []
    count_sample = []
    filter_data = []
    all_dia = []
    all_dia_int = []
    from sklearn.metrics.pairwise import euclidean_distances
    if hand_band_mode == False:
        f_ranges = [[i, i + fit] for i in range(start, stop, step)]
    if hand_band_mode != False:
        f_ranges = sorted(hand_band_mode, key=lambda x: x[0])
    for i in range(len(f_ranges)):
        data_b = filter_func(data, f_ranges[i][0], f_ranges[i][1], filter_mode=filter, spatial_filter=s_filter,
                             order=order)
        X = feach_function(data_b)[:train]
        filter_data.append(X)
        if i == start:
            count_comp.append(len(X[0]))
            count_sample.append(len(X))
        all_dia.append(str(f_ranges[i][0]) + '-' + str(f_ranges[i][1]))
        all_dia_int.append([i, i + fit])
        filter_data_f.append(filter_data)
    f_b = np.array(filter_data_f[0])
    Y = copy.deepcopy(event)
    All_dist = []
    for ranges in range(len(f_b)):
        All_dist_range = []
        Y = Y[:train]
        X = f_b[ranges][:train]
        X_air = X[Y == smell[0]]
        X_drag = X[Y == smell[1]]
        if len(X_air) > len(X_drag):
            D = len(X_air) - len(X_drag)
            X_air = X_air[:len(X_air) - D]
        if len(X_air) < len(X_drag):
            D = len(X_drag) - len(X_air)
            X_drag = X_drag[:len(X_drag) - D]
        if one_comp == False:
            for comp in range(len(X[0])):
                All_dist_range.append([X_air[:, comp][x] - X_drag[:, comp][x] for x in range(len(X_drag))])
            All_dist.append(All_dist_range)
        else:
            All_dist.append(np.diag(euclidean_distances(X_air, X_drag)))
    All_dist = np.array(All_dist)

    print(All_dist.shape, 'vvvvvvvvvvvvvv')
    if one_comp == False:
        distance_plot(All_dist, all_dia, normal, mode, track)
    else:
        distance_all(All_dist, all_dia)
    return

def distance_all(dist, name):
    fig, axes = plt.subplots()
    r = 0
    c = 0
    name = name
    x_min = 0
    x_max = np.ravel(dist).max()
    dist = dist
    for i in range(len(dist)):
        x = np.array(range(len(dist[i])))
        y = np.array([i] * len(dist[i]))
        x1 = dist[i]  # np.linspace(0, 3 * np.pi, 500)
        dydx = (0.5 * (x1[:-1] + x1[1:]))  # first derivative
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        lc = LineCollection(segments, cmap='gray', norm=MidpointNormalize(x_min, x_max, 0.))
        lc.set_array(dydx)
        lc.set_linewidth(5)
        line = axes.add_collection(lc)
        plt.setp(axes,
                 yticks=range(len(name)))
        axes.set_xlim(x.min(), x.max())
        axes.set_ylim(-0.5, i + 0.5)
        labels_y = [item.get_text() for item in axes.get_yticklabels()]
        for j in range(len(labels_y)):
            labels_y[j] = name[j]
        axes.set_yticklabels(labels_y)
        axes.tick_params(labelsize=7.5)
    for i in range(0, len(dist[0]), len(dist[0]) // 4):
        axes.plot([i] * 20, range(0, 20))
    cbar = fig.colorbar(line, ax=axes)
    ticklabs = cbar.ax.get_yticklabels()
    cbar.ax.set_yticklabels(ticklabs, fontsize=6.5)
    plt.show()

def position_visual(data, event, normal='comp', start=1, stop=140, step=10, fit=30, filter='fir', s_filter='None',
                    order=500, train=None, feach_function=corr, hand_band_mode=False):
    data = copy.deepcopy(data)
    filter_data_f = []
    count_comp = []
    count_sample = []
    filter_data = []
    all_dia = []
    all_dia_int = []
    if hand_band_mode == False:
        f_ranges = [[i, i + fit] for i in range(start, stop, step)]
    if hand_band_mode != False:
        f_ranges = sorted(hand_band_mode, key=lambda x: x[0])
    for i in range(len(f_ranges)):
        data_b = filter_func(data, f_ranges[i][0], f_ranges[i][1], filter_mode=filter, spatial_filter=s_filter,
                             order=order)
        X = feach_function(data_b)[:train]
        filter_data.append(X)
        if i == start:
            count_comp.append(len(X[0]))
            count_sample.append(len(X))
        all_dia.append(str(f_ranges[i][0]) + '-' + str(f_ranges[i][1]))
        all_dia_int.append([i, i + fit])
        filter_data_f.append(filter_data)
    f_b = np.array(filter_data_f[0])
    Y = copy.deepcopy(event)
    All_dist = []
    for ranges in range(len(f_b)):
        All_dist_range = []
        Y = Y[:train]
        X = f_b[ranges][:train]
        X_air = X[Y == 64]
        for comp in range(len(X[0])):
            All_dist_range.append(map(lambda x: X_air[:, comp][x], range(len(X_air))))
        All_dist.append(All_dist_range)
    All_dist = np.array(All_dist)
    distance_plot(All_dist, all_dia, normal)
    return


def distance_plot(dist, name, normal='comp', mode=False, track=False,cmap=None):
    c_all = 4
    r_all = (dist.shape[1] // c_all) + 2
    fig, axes = plt.subplots(r_all, c_all, figsize=(3.8 * c_all, 3.8 * r_all), dpi=100)
    r = 0
    c = 0
    name = name
    left, width = .0001, .5
    bottom, height = .25, 0.8
    right = 0
    top = bottom + height
    plt.subplots_adjust(wspace=0.5)
    for comp in range(dist.shape[1]):
        if normal == 'comp':
            x_min = np.min(np.ravel(dist[:, comp]))
            x_max = np.max(np.ravel(dist[:, comp]))
        if normal == 'range':
            x_min = np.ravel(dist).min()
            x_max = np.ravel(dist).max()
        for i in range(len(dist)):
            x = np.array(range(len(dist[i][comp])))
            y = np.array([i] * len(dist[i][comp]))
            x1 = dist[i][comp]  # np.linspace(0, 3 * np.pi, 500)
            dydx = (0.5 * (x1[:-1] + x1[1:]))  # first derivative
            points = np.array([x, y]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            if cmap==None:
                cmap='seismic'
            lc = LineCollection(segments, cmap=cmap, norm=MidpointNormalize(x_min, x_max, 0.))
            lc.set_array(dydx)
            lc.set_linewidth(5)
            line = axes[r, c].add_collection(lc)
            plt.setp(axes[r, c],
                     yticks=range(len(name)))
            axes[r, c].set_xlim(x.min(), x.max())
            axes[r, c].set_ylim(-0.5, i + 0.5)
            labels_y = [item.get_text() for item in axes[r, c].get_yticklabels()]
            for j in range(len(labels_y)):
                labels_y[j] = name[j]
            axes[r, c].set_yticklabels(labels_y)
            axes[r, c].tick_params(labelsize=7.5)
            axes[r, c].set_title(str(comp))
            if mode != False:
                axes[r, 0].text(right, top, mode,
                                horizontalalignment='right',
                                verticalalignment='bottom',
                                transform=axes[r, 0].transAxes)
            A = True
            if track != False:
                if A == True:
                    axes[r, c].plot(range(track[1], len(track[0]) + track[1]), track[0], 'bo', color='green')
                else:
                    axes[r, c].plot(range(track[1], len(track[0]) + track[1]), track[0][:, comp], 'bo', color='green')
        cbar = fig.colorbar(line, ax=axes[r, c])
        ticklabs = cbar.ax.get_yticklabels()
        cbar.ax.set_yticklabels(ticklabs, fontsize=6.5)
        c += 1
        if c == c_all:
            c = 0
            r += 1

    for i in range(r, r_all):
        for j in range(c, c_all):
            fig.delaxes(axes[i, j])
        c = 0
    plt.show()
# distance_plot - вспомогательная функция постройки графиков


def curve_plot(data_for_plot, auc_score):
    fig, axes = plt.subplots()
    fig.set_size_inches(10, 10)
    bottom, height = .25, 0.8
    right = 0
    top = bottom + height
    # print data_for_plot.shape
    for curv_class in range(data_for_plot.shape[0]):
        fpr, tpr, thr = roc_curve(data_for_plot[curv_class][0], data_for_plot[curv_class][1])
        axes.plot(fpr, tpr)
        # axes[0].plot(fpr,fpr)
    axes.legend([str(i) + '-' + 'event' for i in range(1, len(data_for_plot) + 1)], fontsize=12, loc='center left',
                bbox_to_anchor=(1, 0.5))
    axes.set_title('roc auc curve', fontsize=12)
    axes.tick_params(labelsize=10)
    # axes[0].text(right, top, '1,2,3,4',
    # horizontalalignment = 'right',
    # verticalalignment = 'bottom',
    # transform = axes[0].transAxes)
    # axes[0].grid()
    # print auc_score
    matrix = np.vstack([np.array([str(i) + '-' + 'event' for i in range(1, len(data_for_plot) + 1)]), auc_score])
    # collabel = range(1, matrix.shape[1] + 1)
    # rowlabels = range(1, matrix.shape[0] + 1)
    # axes[1].axis('tight')
    # axes[1].axis('off')
    # the_table = axes[1].table(cellText = matrix,
    #                          loc='center',
    #                        )
    plt.show()
    return


def confusion_plot(matrix, matrix_one_all, f_score=None):
    import matplotlib.gridspec as gridspec
    fig = plt.figure(constrained_layout=True)
    fig.set_size_inches(10, 10)
    gs = gridspec.GridSpec(3, len(matrix_one_all), figure=fig)
    subp_names = [i for i in range(len(matrix_one_all))]
    ax1 = plt.subplot(gs[:2, :])
    ax1.set_title('Conf matrix', fontsize=12)
    bottom, height = .25, 0.8
    right = 0
    top = bottom + height
    if len(matrix_one_all) > 1:
        for p in range(len(subp_names)):
            subp_names[p] = plt.subplot(gs[-1, p])
            subp_names[p].imshow(matrix_one_all[0], cmap='cool', interpolation='nearest')
            # subp_names[p].tick_params(labelsize=7.5)
            for i in range(len(range(1, matrix_one_all[p].shape[0] + 1))):
                for j in range(len(range(1, matrix_one_all[p].shape[0] + 1))):
                    text = subp_names[p].text(j, i, matrix_one_all[p][i, j],
                                              ha="center", va="center", color="black")
    ax1.text(right, top,
             'precision=' + str(f_score[0])[:4] + '\n' + 'recal=' + str(f_score[1])[:4] + '\n' + 'f1=' + str(
                 f_score[1])[:4],
             horizontalalignment='right',
             verticalalignment='bottom',
             transform=ax1.transAxes)
    ax1.imshow(matrix, cmap='cool', interpolation='nearest')
    plt.setp(ax1,
             yticks=range(len(matrix)))
    plt.setp(ax1,
             xticks=range(len(matrix)))
    labels_y = [item.get_text() for item in ax1.get_yticklabels()]
    labels_x = [item.get_text() for item in ax1.get_xticklabels()]
    for j in range(len(labels_y)):
        labels_y[j] = range(len(matrix))[j]
        labels_x[j] = range(len(matrix))[j]
    ax1.set_yticklabels(labels_y)
    ax1.set_xticklabels(labels_x)
    ax1.tick_params(labelsize=7.5)
    for i in range(len(range(1, matrix.shape[0] + 1))):
        for j in range(len(range(1, matrix.shape[0] + 1))):
            text = ax1.text(j, i, matrix[i, j],
                            ha="center", va="center", color="black")
    plt.show()
    return


def log_loss_plot(score):
    import warnings
    ll_score = score
    warnings.filterwarnings("ignore")
    fig, axes = plt.subplots()
    fig.set_size_inches(10, 10)
    axes.plot(np.arange(0, 1.1, 0.01), -1 * np.log(np.arange(0, 1.1, 0.01)), color='blue')
    # print -1 * np.log(np.arange(0, 1.1, 0.01))
    # print ll_score - 0.01,ll_score + 0.01
    mask_signal = np.all([[(-1 * np.log(np.arange(0, 1.1, 0.01)) >= ll_score - 0.05)],
                          [(-1 * np.log(np.arange(0, 1.1, 0.01)) <= ll_score + 0.05)]], axis=0)[0]
    # print mask_signal
    axes.scatter(np.arange(0, 1.1, 0.01)[np.where(
        -1 * np.log(np.arange(0, 1.1, 0.01)) == -1 * np.log(np.arange(0, 1.1, 0.01))[mask_signal][0])]
                 , -1 * np.log(np.arange(0, 1.1, 0.01))[mask_signal][0], color='red')
    plt.show()
    return


def cv_ac_score_plot(score, score_mean):
    fig, axes = plt.subplots()
    axes.bar(range(len(score)), score, color='blue')
    axes.bar([len(score) + 1], score_mean, color='orange')
    plt.show()
    return


def visual_report():
    c_all = 6 + 2
    r_all = (28 // c_all) + 2 + 2
    fig, axes = plt.subplots(r_all, c_all, figsize=(2 * c_all, 2 * r_all), dpi=100)
    plt.show()
    return


def full_visual(data, event, chunk=False, bust_targets=False, num_class=2, *args):
    data = copy.deepcopy(data)
    feachers_met = ['corr', 'fft', 'hj']
    met_all = {'corr': corr, 'fft': fft, 'hj': hj}
    cat = np.unique(event)
    for f_m, num in zip(feachers_met, range(len(feachers_met))):
        feacher_method = met_all.get(f_m)
        if f_m == 'fft':
            X, meta = feacher_method(data, *args)
        X, meta = feacher_method(data)
        compression(X, event, mode='tsne', title=f_m)
        if chunk == False:
            for i in range(2, 10):
                if len(X) % i == 0:
                    chunk_gen = [len(X) // i] * (len(X) // (len(X) // i))
                    break
            compression(X, event, chunk=chunk_gen, time=True, title=f_m)
            compression_by_ser(X, event, chunk=chunk_gen, title=f_m)
        else:
            compression(X, event, chunk=chunk_gen, time=True, title=f_m)
            compression_by_ser(X, event, chunk=chunk_gen, title=f_m)
        for num_class in range(2, 4):
            X_BUST, Y_BUST, N_C, EL = bust_classes(X, event, catage=cat, number_classes=num_class)
            for x, y, tar in zip(X_BUST, Y_BUST, EL):
                print(list(tar))
                visual_comp_matrix(x, y, list(tar), title=f_m, f_metadata=meta)
        # TO DO
        # добавить графики корреляций компонент между собой, берется две компоненты и отображаются в зависимости от друг друга

    return


def drow_epoch_all(dir, sub, par_targets=[1, list(range(1, 12, 1)), 1]):  # pokazivaet usrednennoe po vsem epoham
    inf = data_to_epochs(dir, sub, par_targets=par_targets)
    nubmer_of_epochs = len(inf)  # chislo epoh soglasno usloviy par_targets
    all_epach = inf.average()  # usrednenie
    all_epach.plot_topomap()
    return all_epach, nubmer_of_epochs


def drow_epoch_each(s, dir, sub, par_targets=[1, list(range(1, 12, 1)),
                                              1]):  # pokazivaet odnu vabrannuy usrenennuy epohu, s - nomer epohi
    inf = data_to_epochs(dir, sub, par_targets=par_targets)
    nubmer_of_epochs = len(inf)
    one_epoch = inf[s].average()
    one_epoch.plot_topomap()
    return one_epoch, nubmer_of_epochs


def drow_epoch_group(s, dir, sub,
                     par_targets=[1, list(range(1, 12, 1)), 1]):  # pokazivaet usrenennuy gruppu epohu, s-shag iterachii
    inf = data_to_epochs(dir, sub, par_targets=par_targets)
    nubmer_of_epochs = len(inf)
    for i in range(1, nubmer_of_epochs):
        st = inf[s * (i - 1):i * s].average()
        st.plot_topomap()
        if (i * s) > nubmer_of_epochs - 1:
            break
        else:
            continue
    return nubmer_of_epochs, st


def morlet_wavelet(n_cycles, freqs, N_Channel, n_jobs, decim, return_itc, inf):  # morlet's vawelet presentation
    # n_cycles = 1   The number of cycles globally or for each frequency.
    # freqs = np.arange(1., 40., 1.)   The intresting frequencies in Hz
    # Chose of chanel N_Channel
    # vmin, vmax = -10000., 10000. # Define our color limits.
    # n_jobs The number of jobs to run in parallel
    # itc The inter-trial coherence (ITC). Only returned if return_itc is True.
    # decim slice, defaults to 1 To reduce memory usage, decimation factor after time-frequency decomposition
    # primer visova s,d=data_to_mne.morlet_wavelet(inf=inf,n_cycles=1,freqs = np.arange(1., 40., 1.),N_Channel=0,return_itc=True, decim=3, n_jobs=1)
    power, itc = tfr_morlet(inf, freqs=freqs, n_cycles=n_cycles, return_itc=True, decim=3, n_jobs=1)
    power.plot([N_Channel])
    return power, itc


def mne_visual(data):
    sfreq = 1000  # Sampling frequency
    ch_types = ['eeg'] * data.shape[1]
    ch_names = [str(i) for i in range(data.shape[1])]
    info = mne.create_info(ch_names=ch_names, sfreq=sfreq, ch_types=ch_types)
    data_mne = np.zeros((data.shape[1], data.shape[0] * data.shape[2]))
    for ch in range(data.shape[1]):
        data_mne[ch] = np.ravel(data[:, ch, :])

    raw_obj = mne.io.RawArray(data_mne, info)
    scalings = {'eeg': 20e-6}  # 'auto'
    raw_obj.plot(n_channels=data.shape[1], scalings=scalings, title='Data from arrays',
                 show=True, block=True)
    return

def slice_distance_viewer(X, Y, smell=16,window=3000,fit=100,normal='comp',f1=1,f2=3,f='fft',freq=250):
    data = X[Y==smell]
    smooth_parts = np.array([[i, i + window] for i in range(0, data.shape[2]-(window)+fit, fit)])
    name = [str(i[0])+'-'+str(i[1]) for i in smooth_parts]

    def fft(or_data):
        #print or_data.shape,'or_data shape'
        sfreq = freq
        N = or_data.shape[2]

        point = np.zeros((or_data.shape[1]))
        for ch in range(or_data.shape[1]):
            spectrum =np.abs(np.fft.rfft(or_data[0][ch]))
            freqs = np.fft.rfftfreq(N, 1. / sfreq)
            mask_signal = np.all([[(freqs >= f1)], [(freqs <= f2)]], axis=0)[0]

            Zxx = spectrum[mask_signal]

            point[ch]=np.mean(Zxx)
        return point # возвращается массив точек с 1 значением по размеру количества каналов
    def cov(or_data):
        coef = [np.triu(np.asmatrix((np.cov(or_data[i]))), 1)[np.triu(np.asmatrix((np.cov(or_data[i]))), 1) != 0]
                for i in range(len(or_data))]
        coef = np.array(coef)
        return coef
    def corr(or_data):
        coef = [np.triu(np.asmatrix((np.corrcoef(or_data[i]))), 1)[np.triu(np.asmatrix((np.corrcoef(or_data[i]))), 1) != 0]
                for i in range(len(or_data))]
        coef = np.array(coef)
        return coef
    if f=='corr':
        f=corr
        All_data = np.zeros((smooth_parts.shape[0], ((data.shape[1]*data.shape[1])-data.shape[1])//2, data.shape[0]))
    if f=='fft':
        f=fft
        All_data = np.zeros((smooth_parts.shape[0], data.shape[1], data.shape[0]))
    if f=='cov':
        All_data = np.zeros(
            (smooth_parts.shape[0], ((data.shape[1] * data.shape[1]) - data.shape[1]) // 2, data.shape[0]))
        f=cov
    for x in range(data.shape[0]):
        base_fft_data = f(np.array([data[x,:,:window]]))
        for step,step_value in zip(smooth_parts,range(smooth_parts.shape[0])):
            slice_fft_data = f(np.array([data[x, :, step[0]:step[1]]]))
            dif = base_fft_data-slice_fft_data
            All_data[step_value,:,x]=dif#base_fft_data-slice_fft_data
    distance_plot(All_data, name, normal, mode=False, track=False)
    return

def spectral_entropy_viewer(X, Y, smell=16,window=3000,fit=100,normal='comp',f1=1,f2=3):
    data = X[Y==smell]
    smooth_parts = np.array([[i, i + window] for i in range(0, data.shape[2]-(window)+fit, fit)])
    name = [str(i[0])+'-'+str(i[1]) for i in smooth_parts]

    def psd_entropy(or_data):
        point = np.zeros((or_data.shape[1]))
        for ch in range(or_data.shape[1]):
            freqs, psd = signal.welch(or_data[0][ch],fs=250)
            mask_signal = np.all([[(freqs >= f1)], [(freqs <= f2)]], axis=0)[0]
            Zxx = np.abs(psd)[mask_signal]

            point[ch]=stats.entropy(Zxx,base=2)
        return point # возвращается массив точек с 1 значением по размеру количества каналов


    f=psd_entropy
    All_data = np.zeros((smooth_parts.shape[0], data.shape[1], data.shape[0]))

    for x in range(data.shape[0]):
        base_fft_data = f(np.array([data[x,:,:window]]))
        for step,step_value in zip(smooth_parts,range(smooth_parts.shape[0])):
            slice_fft_data = f(np.array([data[x, :, step[0]:step[1]]]))
            dif =slice_fft_data
            All_data[step_value,:,x]=dif#base_fft_data-slice_fft_data
    distance_plot(All_data, name, normal, mode=False, track=False,cmap='viridis')
    return