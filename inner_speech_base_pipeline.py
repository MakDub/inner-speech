import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
from sklearn.base import TransformerMixin, BaseEstimator
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis as LDA
from sklearn.ensemble import RandomForestClassifier as RF
from sklearn.linear_model import LogisticRegression as LR
from sklearn.metrics import confusion_matrix, accuracy_score
from sklearn.neighbors import KNeighborsClassifier as KN
from sklearn.neural_network import MLPClassifier as MLP
from sklearn.pipeline import make_pipeline

from Reader import EDF_data_reader
from pyeeg import hjorth


class Features(BaseEstimator, TransformerMixin):
    def __init__(self, function, mode):
        self.f = function
        self.mode = mode

    def fit(self, X, y):
        return self

    def transform(self, X, y=None):
        def corr(data):  # подготавливает выборку признаков crosscorr
            coef = [
                np.triu(np.asmatrix((np.corrcoef(data[i]))), 1)[np.triu(np.asmatrix((np.corrcoef(data[i]))), 1) != 0]
                for i in range(len(data))]
            coef = np.array(coef)
            return coef

        def cov(data):  # подготавливает выборку признаков cov
            coef = [
                np.triu(np.asmatrix((np.cov(data[i]))), 1)[np.triu(np.asmatrix((np.cov(data[i]))), 1) != 0]
                for i in range(len(data))]
            coef = np.array(coef)
            return coef

        def hj(data):  # подготавливает выборку признаков hj
            ch = data.shape[1]
            data = np.array([[hjorth(data[event][channel]) for channel in range(ch)] for event in range(data.shape[0])])
            data = data.reshape(data.shape[0], data.shape[1] * data.shape[2])
            return data

        def var(data):
            ch = data.shape[1]
            data = np.array([[np.var(data[event][channel]) for channel in range(ch)] for event in range(data.shape[0])])
            return data

        def fft(data, freq=250, band=False, start=1, stop=70, step=10,
                fit=30):  # подготавливает выборку признаков fft
            if band == False:
                freq_band = [[i, i + fit] for i in range(start, stop, step)]
            else:
                freq_band = sorted(band, key=lambda x: x[0])

            def fft(data, freq, dia_from, dia_to):
                sfreq = freq
                ch = data.shape[1]
                N = data.shape[2]
                spectrum = np.abs((np.fft.rfft(data)))
                freqs = np.fft.rfftfreq(N, 1. / sfreq)
                mask_signal = np.all([[(freqs >= dia_from)], [(freqs <= dia_to)]], axis=0)[0]
                data = np.array(
                    [[np.mean(spectrum[event][channel][mask_signal]) for channel in range(ch)] for event in
                     range(data.shape[0])])
                return data

            start = fft(data, freq, freq_band[0][0], freq_band[0][1])
            if len(freq_band) > 1:
                for i in range(1, len(freq_band)):
                    end = fft(data, freq, freq_band[i][0], freq_band[i][1])
                    array_stack = np.hstack((start, end))
                    start = array_stack
                return array_stack
            else:
                return start

        def hjstft(data, fs=250, band=[1, 3]):
            freqs, t, Zxx = signal.stft(data, fs)
            mask_signal = np.all([[(freqs >= band[0])], [(freqs <= band[1])]], axis=0)[0]
            data = np.mean(np.abs(Zxx[:, :, mask_signal, :]), axis=2)
            data = hj(data)
            return data

        def corstft(data, fs=250, band=[1, 3]):
            freqs, t, Zxx = signal.stft(data, fs)
            mask_signal = np.all([[(freqs >= band[0])], [(freqs <= band[1])]], axis=0)[0]
            data = np.mean(np.abs(Zxx[:, :, mask_signal, :]), axis=2)
            data = corr(data)
            return data

        def covstft(data, fs=250, band=[1, 3]):
            freqs, t, Zxx = signal.stft(data, fs)
            mask_signal = np.all([[(freqs >= band[0])], [(freqs <= band[1])]], axis=0)[0]
            data = np.mean(np.abs(Zxx[:, :, mask_signal, :]), axis=2)
            data = cov(data)
            return data

        if self.mode == 'base' or self.mode == 'spatial_filter':  # работа pipeline по схеме Features -> Classifier
            if self.f == 'cor':
                X = corr(X)
                return X
            if self.f == 'hj':
                X = hj(X)
                return X
            if self.f == 'cov':
                X = cov(X)
                return X
            if self.f == 'fft':
                X = fft(X)
                return X
            if self.f == 'hjstft':
                X = hjstft(X)
                return X
            if self.f == 'corstft':
                X = corstft(X)
                return X
            if self.f == 'covstft':
                X = covstft(X)
                return X
            if self.f == 'var':
                X = var(X)
                return X


class Spatial_filter(BaseEstimator, TransformerMixin):

    def __init__(self, s_filter=None, matrix=[[-1, 0, -1, 1, -1],
                                              [2, 3, 4, 5, 6],
                                              [7, 8, 9, 10, 11],
                                              [12, 13, 14, 15, 16]]):
        self.s_f = s_filter
        self.matrix = matrix

    def fit(self, X, y):
        return self

    def transform(self, X, y=None):
        if self.s_f == None:
            return X
        if self.s_f == 'car':
            for i in range(X.shape[1]):
                X[:, i, :] = np.subtract(X[:, i, :], np.mean(X, axis=1))
            return X
        if self.s_f == 'small_laplacian':
            mat = np.array(self.matrix)

            for row in range(mat.shape[0]):
                for col in range(mat.shape[1]):
                    if mat[row][col] != -1:
                        configuration_electrod = np.ones((5)) * (-1)
                        row_shift_down = row - 1
                        row_shift_up = row + 1
                        col_shift_left = col - 1
                        col_shift_right = col + 1
                        configuration_electrod[0] = mat[row][col]
                        if col_shift_left >= 0 and mat[row][col_shift_left] != -1:
                            configuration_electrod[1] = mat[row][col_shift_left]
                        if col_shift_right < mat.shape[1] and mat[row][col_shift_right] != -1:
                            configuration_electrod[2] = mat[row][col_shift_right]
                        if row_shift_up < mat.shape[0] and mat[row_shift_up][col] != -1:
                            configuration_electrod[3] = mat[row_shift_up][col]
                        if row_shift_down >= 0 and mat[row_shift_down][col] != -1:
                            configuration_electrod[4] = mat[row_shift_down][col]
                        configuration_electrod = configuration_electrod[configuration_electrod != -1].astype(int)
                        X[:, configuration_electrod[0], :] = np.subtract(X[:, configuration_electrod[0], :],
                                                                         np.mean(X[:, configuration_electrod[1:], :],
                                                                                 axis=1))
            return X


class Anomaly_detection(BaseEstimator, TransformerMixin):

    def __init__(self):
        pass

    def fit(self, X, y):
        return self

    def transform(self, X, y=None):
        return


class Augmentation(TransformerMixin, BaseEstimator):

    def __init__(self):
        pass

    def fit(self, X, y):
        return self

    def transform(self, X, y=None):
        return


def inner_speech_recognition(f_n, f='cov', mode='base'):
    report_data = []
    X, y, m = EDF_data_reader(file_name=f_n, mode='Naive', voice_tipe='inner', add_mio=False)
    models = [LR(), LDA(), KN(), RF(random_state=1), MLP(hidden_layer_sizes=(16, 6), solver='lbfgs', random_state=1)]
    for m in models:
        for f in ['cov', 'cor', 'hj', 'fft', 'hjstft', 'corstft', 'covstft', 'var']:
            model = m
            if mode == 'base':  # работа pipeline по схеме Features -> Classifier
                pipe = make_pipeline(Spatial_filter(s_filter=None), Features(function=f, mode=mode),
                                     model)
            if mode == 'spatial_filter':
                pipe = make_pipeline(Spatial_filter(s_filter='small_laplacian'), Features(function=f, mode=mode),
                                     model)
            pipe.fit(X[:len(X) // 2], y[:len(X) // 2])
            y_pred = pipe.predict(X[len(X) // 2:])
            c_m = confusion_matrix(y[len(X) // 2:], y_pred)
            a_s = accuracy_score(y[len(X) // 2:], y_pred)
            print(c_m)
            print(a_s)
            report_data.append([f_n, str(model).split('(')[0], c_m, str(a_s)[:6], str(f)])
    return report_data, mode


def report_pdf(data, mode):
    from fpdf import FPDF
    if mode == 'base':  # работа pipeline по схеме Features -> Classifier и отчет к данной схеме
        pdf = FPDF()
        score = []
        features_name = ['cov', 'cor', 'hj', 'fft', 'hjstft', 'corstft', 'covstft', 'var']
        legend = []
        pdf.add_page()
        pdf.set_font("Arial", size=12)
        pdf.cell(200, 10, txt=data[0][0], ln=1, align="C")
        for data in data:
            pdf.cell(200, 12, txt=data[1], ln=1, align="L")
            if data[4] in features_name:
                pdf.set_font("Arial", size=12, style='B')
                pdf.cell(200, 14, txt='Feature: ' + data[4], ln=1, align="L")
                pdf.set_font("Arial", size=12)
            pdf.cell(200, 16, txt=data[3] + '-accuracy score', ln=1, align="L")
            pdf.cell(200, 16, txt='Confusion matrix', ln=1, align="L")
            col_width = pdf.w / 20
            row_height = pdf.font_size
            score.append(float(data[3]))
            legend.append(data[1] + '-' + data[4])
            for row in data[2]:
                for item in row:
                    pdf.cell(col_width, row_height * 1,
                             txt=str(item), border=2, align="C")
                pdf.ln(row_height * 1)
        colors = ['red', 'green', 'blue', 'pink', 'orange']
        color_array = [[colors[i] for j in range(len(features_name))] for i in range(5)]
        fig = plt.figure(figsize=(12, 12))
        ax = fig.add_subplot(111)
        ax.bar(legend, score, align='edge', bottom=0.0, color=np.ravel(color_array))
        ax.set_xticklabels(legend, rotation=45, rotation_mode="anchor", ha="right")
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(8)
        plt.savefig('report_figure.png', format='png')
        pdf.add_page()
        pdf.image('report_figure.png', x=10, y=8, w=170)
        pdf.ln(85)
        pdf.output(data[0] + '-' + mode + '.pdf')
        return


data = inner_speech_recognition('Anton.EDF', mode='spatial_filter')
# X, y, m = EDF_data_reader(file_name='BAKAEVA.EDF', mode='Naive', voice_tipe='out', add_mio=False)
