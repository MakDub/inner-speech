# coding=utf-8
import itertools

import numpy as np
from sklearn.model_selection import cross_val_score


def bust_classes(X, y, catage, number_classes=2):
    X_BUST, Y_BUST, N_C, EL, X_BUST_TEST = [], [], [], [], []
    for element in itertools.combinations(catage, number_classes):
        i_select_class = np.array([y == c for c in np.array(element)]) * 1
        my_class = reduce(lambda x, y: (x) + (y), i_select_class).astype(bool)
        X_bust, Y_bust, X_bust_test = X[my_class], y[my_class], X[my_class]
        X_BUST.append(X_bust)
        Y_BUST.append(Y_bust)
        N_C.append(number_classes)
        EL.append(element)
        X_BUST_TEST.append(X_bust_test)
    return X_BUST, Y_BUST, N_C, EL


def splitter(data, n):
    return


def skew(event):
    return


def holdout_method(data_sc, data_all, event_all, event, h_m_rand=False, test_vol=30):
    N = data_sc.shape[0]
    N_t = int((1 - (test_vol / 100)) * N)
    if h_m_rand == True:
        indx = np.random.permutation(N)
        indx_train, indx_test = indx[:N_t], indx[N_t:]
        data_train, data_test = data_sc[indx_train, :], data_sc[indx_test, :]
        event_train, event_test = event[indx_train], event[indx_test]
        data_train_all, data_test_all = data_all[indx_train, :], data_all[indx_test, :]
        event_train_all, event_test_all = event_all[indx_train], event_all[indx_test]
    else:
        indx_train = N_t
        data_train, data_test = data_sc[:indx_train], data_sc[indx_train:]
        event_train, event_test = event[:indx_train], event[indx_train:]
        data_train_all, data_test_all = data_all[:indx_train], data_all[indx_train:]
        event_train_all, event_test_all = event_all[:indx_train], event_all[indx_train:]
    return data_train, event_train, data_test, event_test, data_train_all, data_test_all, event_train_all, event_test_all


def cross_val_method(data, event, estimator, cv=3, mode='MK_cv',
                     test_vol=30):  # cv только на обучающей и тестовой или можно в целом и вместе?
    if mode == 'q-fold':
        score = cross_val_score(estimator, data, event, cv=cv)
        score_mean = np.mean(score)
        return score_mean, score
    if mode == 'MK_cv':
        # выбираем объем тестовой выборки, формируем ее рандомными числами из сета, прогоняем,
        N = data.shape[0]
        N_t = int((1 - (test_vol / 100)) * N)
        indx_train = N_t
        indx = np.random.permutation(N)
        #print
        #indx, len(indx)
        #print
        # итак нужно реализовать монтекарло при этом сделать его с замутом под пиплайн и gridsearchcv
        return

def Kalman1D(X, r):
   from pykalman import KalmanFilter
   Time = range(len(X))
   X = X

   #print np.cov(r)
   # Filter Configuration

   # time step
   # dt = Time[1] - Time[0]
   #r=np.cov(X)

   # transition_matrix
   F = [[1]]

   # observation_matrix
   H = [1]

   # transition_covariance
   Q = [[1]]

   # observation_covariance
   R = [1]  # max error = 0.6m

   # initial_state_mean
   X0 = [X[0]]



   n_timesteps = len(Time)
   n_dim_state = 1

   P0 = [[r]]
   R = [r]
   filtered_state_means = np.zeros((n_timesteps, n_dim_state))
   filtered_state_covariances = np.zeros((n_timesteps, n_dim_state, n_dim_state))
   # Kalman-Filter initialization
   kf = KalmanFilter(
           # observation_matrices=H,
           # transition_covariance=Q,
           observation_covariance=R,
           initial_state_mean=X[0],
           # initial_state_covariance=P0,
           em_vars=['transition_matrix', 'observation_matrix', 'transition_covariance', 'initial_state_covariance',
                    'transition_offsets'
                    ])
   filtered_state_means[0] = X[0]
   filtered_state_covariances[0] = P0
   # iterative estimation for each new measurement
   for t in range(1, n_timesteps):
       filtered_state_means[t], filtered_state_covariances[t] = (
               kf.filter_update(
                   filtered_state_mean=filtered_state_means[t - 1],
                   filtered_state_covariance=filtered_state_covariances[t - 1],
                   observation=X[t])
           )


   import matplotlib.pyplot as plt
   print (Time, filtered_state_means[:, 0])
   position_sigma = np.sqrt(filtered_state_covariances[:, 0, 0])
   plt.plot(Time, filtered_state_means[:, 0], "g-", label="Filtered position", markersize=1)
   plt.plot(Time, filtered_state_means[:, 0] + position_sigma, "r--", label="+ sigma", markersize=1)
   plt.plot(Time, filtered_state_means[:, 0] - position_sigma, "r--", label="- sigma", markersize=1)
   plt.plot(Time, X)
   plt.grid()
   plt.legend(loc="upper left")
   plt.xlabel("Time (loop)")
   plt.ylabel("Position")
   plt.show()


   return filtered_state_means[:, 0]
