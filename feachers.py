# coding=utf-8
import copy
import itertools

import numpy as np
from mne.time_frequency import tfr_array_morlet

from pyeeg import hjorth


def sample_corr(data):  # подготавливает выборку признаков crosscorr
    ch_list = ['F3', 'F4', 'C3', 'C4', 'P3', 'P4']
    meta_data = []
    for element in itertools.combinations(ch_list, 2):
        meta_data.append(element[0] + '-' + element[1])
    meta_data = np.array(meta_data)
    coef = [np.triu(np.asmatrix((np.corrcoef(data[i]))), 1)[np.triu(np.asmatrix((np.corrcoef(data[i]))), 1) != 0]
            for i in range(len(data))]
    coef = np.array(coef)
    return coef


def fft(data, freq=250, band=False, start=1, stop=70, step=10,
        fit=30):  # подготавливает выборку признаков fft
    if band == False:
        freq_band = [[i, i + fit] for i in range(start, stop, step)]
    else:
        freq_band = sorted(band, key=lambda x: x[0])

    def fft(data, freq, dia_from, dia_to):
        sfreq = freq
        ch = data.shape[1]
        N = data.shape[2]
        spectrum = np.abs((np.fft.rfft(data)))
        freqs = np.fft.rfftfreq(N, 1. / sfreq)
        mask_signal = np.all([[(freqs >= dia_from)], [(freqs <= dia_to)]], axis=0)[0]
        data = np.array(
            [[np.mean(spectrum[event][channel][mask_signal]) for channel in range(ch)] for event in
             range(data.shape[0])])
        return data

    start = fft(data, freq, freq_band[0][0], freq_band[0][1])
    if len(freq_band) > 1:
        for i in range(1, len(freq_band)):
            end = fft(data, freq, freq_band[i][0], freq_band[i][1])
            array_stack = np.hstack((start, end))
            start = array_stack
        return array_stack
    else:
        return start


def sample_hjorth(data):  # подготавливает выборку признаков hj
    ch = data.shape[1]
    data = np.array([[hjorth(data[event][channel]) for channel in range(ch)] for event in range(data.shape[0])])
    data = data.reshape(data.shape[0], data.shape[1] * data.shape[2])
    return data


def features_from_wavelet(inf, n_cycles=1, freqs=np.arange(1., 50., 1.)):  # morlet's vawelet to features
    # n_cycles = 1   The number of cycles globally or for each frequency.
    # freqs = np.arange(1., 40., 1.)   The intresting frequencies in Hz
    # Chose of chanel N_Channel
    # vmin, vmax = -10000., 10000. # Define our color limits.
    # n_jobs The number of jobs to run in parallel
    # itc The inter-trial coherence (ITC). Only returned if return_itc is True.
    # decim slice, defaults to 1 To reduce memory usage, decimation factor after time-frequency decomposition

    # до этого функция возвращала  EpochsTFR хрен знает как это в классификатор засовывать, поэтому исправлено
    power = tfr_array_morlet(inf, sfreq=1024, freqs=freqs, n_cycles=n_cycles, decim=3, n_jobs=1, output='power')
    event = np.ravel(inf.events[:, 2:])  # это эвенты из эпох
    # теперь функция создает 4-ех мерный массив, принтонув shape увидишь (39L, 6L, 49L, 1366L) нужно из этого получить двумерный
    # как это сделать можно спросить у Антона
    print
    power.shape
    return power, event


'''
старая функция
def features_from_wavelet(n_cycles,freqs,N_Channel,n_jobs,decim,return_itc,inf): #morlet's vawelet to features
    # n_cycles = 1   The number of cycles globally or for each frequency.
    # freqs = np.arange(1., 40., 1.)   The intresting frequencies in Hz
    #Chose of chanel N_Channel
    # vmin, vmax = -10000., 10000. # Define our color limits.
    # n_jobs The number of jobs to run in parallel
    # itc The inter-trial coherence (ITC). Only returned if return_itc is True.
    # decim slice, defaults to 1 To reduce memory usage, decimation factor after time-frequency decomposition
    power, itc = tfr_morlet(inf, freqs=freqs, n_cycles=n_cycles, return_itc=True, decim=3, n_jobs=1)
    return power, itc
'''
